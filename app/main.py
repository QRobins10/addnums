from typing import Optional

from fastapi import FastAPI
import redis
import time

app = FastAPI()
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)



@app.get("/")
def read_root():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)


@app.get("/{first_num}")
def read_item(first_num: int):
    return {"sum": first_num + 10}


@app.get("/{first_num}/{second_num}")
def read_item(first_num: int, second_num: int):
    return {"sum": first_num + second_num}
