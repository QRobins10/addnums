FROM tiangolo/uvicorn-gunicorn-fastapi:latest

COPY ./app /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt